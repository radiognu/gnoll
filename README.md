# Gnoll, el bot IRC de RadioÑú

- - -

## ¿Qué es Gnoll?

Es un bot IRC de RadioÑú que tiene algunas funciones. Su comportamiento es muy
antisocial.
De hecho, su nombre de debe a la mezcla de las palabras **GNU** y **Troll**.

## ¿Cómo configurar el bot?

Primero que nada se debe editar el archivo "lib/configuracion.php" y cambiar las
siguientes variables (servidor, puerto, nick, canal)

```
#!php

$conf['host']     = "irc.radiognu.org";
$conf['port']     = 6667;
$conf['canal']    = "#radiognu";
$conf['nick']     = "Gnoll";
$conf['pass']     = "AQUI-LA-CLAVE-DEL-NICKSERV";
$conf['admins'][] = "nick-de-un-admin";
$conf['admins'][] = "nick-de-un-admin";
$conf['admins'][] = "nick-de-un-admin";
$conf['admins'][] = "nick-de-un-admin";
```

## ¿Cómo ejecutar el bot?

Solo se necesitan sencillos comandos (comandos de consola) para hacer funcionarlo.
Para ejecutar el bot:

```
./ctl_gnoll.sh --iniciar
```

Para detenerlo:

```
./ctl_gnoll.sh --detener
```

Y para reiniciar:

```
./ctl_gnoll.sh --reiniciar
```
