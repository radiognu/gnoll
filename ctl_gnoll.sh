#!/bin/sh

USO="Comandos disponibles:\n
--iniciar\t  \t      \t      nombra que procesos estan activos en la radio\n
--detener\t \t      \t      levanta el servicio ICECAST\n
--reiniciar\t  \t      \t      levanta el servicio LIQUIDSOAP\n"

if [ -z $1 ] ; then echo -e $USO; exit; fi

#PIDGNOLL=`ps aux | grep gnoll | grep -v "grep" | grep  "gnoll" | awk '{print $2}'`

PIDGNOLL=`ps -C "php gnoll.php" | grep php | cut -f 1 -d " "`

if [ -z $1 ] ; then echo -e $USO; exit; fi

if [ $1 == '--iniciar' ] ; then
    nohup php gnoll.php &
fi

if [ $1 == '--detener' ] ; then
    kill -9 $PIDGNOLL
fi

if [ $1 == '--reiniciar' ] ; then
    kill -9 $PIDGNOLL;
    nohup php gnoll.php &
fi
