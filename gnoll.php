<?php

/*
 * gnoll.php -- Gnoll 4.0, el bot de RadioGNU
 *
 * Copyright 2010 - 2014 Octavio Rossell <octavio@gnu.org.ve>
 *                       Tomás Vielma    <contacto@tomivs.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

// todo un rollo el tema del CHARSET. Con esto nos aseguramos de la codificación correcta
$x = array("ASCII", "UTF-8", "ISO-8859-1");
mb_detect_order($x);

// para que PHP no detenga el script en 30s
set_time_limit(0);

// nos traemos las funciones
Require "./lib/identica.php";
Require "./lib/funciones.php";
Require "./lib/configuracion.php"

// variables de depuracion
$conf['debug']['recv'] = 1;
$conf['debug']['rarr'] = 1;
$conf['debug']['leer'] = 0;
$conf['debug']['send'] = 1;
$conf['debug']['priv'] = 0;

$SOCKET = @fsockopen($conf['host'], $conf['port'], $errno, $errstr, 2);

if($SOCKET) {
    send("PASS NOPASS\n"); // no es necesaria para la mayoria de servidores
    send("NICK {$conf['nick']}\n");
    send("USER {$conf['nick']} USING PHP IRC\n"); // envia el usuario, debe tener cuatro parametros
    while(!feof($SOCKET)) {
        $BUFFER = fgets($SOCKET, 1024);
        if (mb_detect_encoding($BUFFER) != "UTF-8") { $BUFFER=utf8_encode($BUFFER); }
        $in = split (" ", $BUFFER, 4);
        // DEBUG
        if ($conf['debug']['recv'] == 1) { echo mb_detect_encoding($BUFFER)." - [RECIBIENDO]: $BUFFER"; }
        if ($conf['debug']['rarr'] == 1) { print_r ($in); }
        // mantenemos el servidor respondiendo con PONG
        if($in[0] == "PING") {
            // mantenemos el servidor respondiendo con PONG
            send("PONG {$in[1]}\n");
        } else {
            // si no es una solicitud de PING, procesa la entrada
            leer_socket ($in);
        }
        // acá borramos el BUFFER para que el texto del loop (WHILE)  se vea "por demanda"
        flush();
    }
} else {
    echo "ERROR: no me pude conectar al servidor, qué chimbo :-( $errstr ($errno)\n";
}
?>
