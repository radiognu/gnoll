<?php

/*
 * configuracion.php -- Variables de configuracion de Gnoll
 *
 * Copyright 2010 - 2014 Octavio Rossell <octavio@gnu.org.ve>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

// iniciamos los objetos para enviar DENTS:
$identica_radiognu = new Identica("usuario", "contrasena");

$conf = array();
$conf['host']     = "127.0.0.1";
$conf['port']     = 6667;
$conf['canal']    = "#canal";
$conf['nick']     = "Gnoll";
$conf['pass']     = "MiGranContrasenaMacoy123";
$conf['admins'][] = "Admin1";
$conf['admins'][] = "Admin2";
$conf['admins'][] = "Admin3";
$conf['admins'][] = "Admin4";
?>