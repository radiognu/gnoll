<?php

/*
 * funciones.php -- Funcionalidades para Gnoll
 *
 * Copyright 2010 - 2014 Octavio Rossell <octavio@gnu.org.ve>
 *                       Tomás Vielma    <contacto@tomivs.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/*
RED = '\x035'
LRED = '\x034'
GREEN = '\x033'
LGREEN = '\x039'
BLUE = '\x032'
LBLUE ='\x0312'
CYAN = '\x0310'
LCYAN = '\x0311'
YELLOW = '\x038'
ORANGE = '\x037'
MAGENTA = '\x036'
LMAGENTA = '\x0313'
*/

function leer_socket ($in) {
    global $conf;
    if ($conf['debug']['leer'] == 1) { print_r ($in); }
    if (isset($in[1])) {
        $n = split ("!", mb_substr(trim($in[0]), 1));
        $nick = $n[0];
        if (isset($in[3])) { $msg = mb_substr(trim($in[3]), 1); }
        $c = mb_substr(trim($in[2]), 1);
        switch ($in[1]) {
            // si alguien dice algo en el canal lo analizamos
            case "PRIVMSG":
                if (substr($in[2], 0, 1) == "#") { revisar ($nick, $msg); }
                else {
                    send ("PRIVMSG $nick no respondo a mensajes privados\n");
                }
            break;
            // eventos a los cuales atender
            case "QUIT": saliendo($nick); break;
            case "PART": saliendo($nick); break;
            case "JOIN": entrando($nick); break;
            case "NICK": cambionk($nick, $c); break;
            case "266":  send("PRIVMSG nickserv :IDENTIFY {$conf['pass']}\n"); send("CYCLE {$conf['canal']}\n"); break;
            case "353":
                global $quienes;
                $q = split(":", $in[3]);
                $quienes = $q[1];
            break;
        }
    }
}

function revisar($nick, $msg) {
    global $conf;
    $gnollnick = $conf['nick'];
    if ($msg[0] == "!") { comando($nick, $msg); return; }
    if (preg_match("/$gnollnick/i", $msg)) { responder($nick, $msg); return; }
    reconocer($nick, $msg);
    loguear ($nick, $msg);
    return;
}

function saliendo($nick) {
    //priv ("se fue $nick del canal. "); return;
}

function entrando($nick) {
    $saludo ="";
    switch ($nick) {
        case "marieche":
            $saludo = "Salve gran marieche, oh adorable, venerable y excelentísimia señora doña Cuaima del Norte";
        break;
        case "BreadMaker":
            $saludo = "BreadMaker: ha llegado nada más y nada menos que el CE TE EME!!! ahí no má!!!";
        break;
    }
    if ($saludo != "") priv($saludo); return;
}

function cambionk($nick, $newnick){
    //priv ("$nick ha entrado al canal, si, yo se que es obvio, estoy probando "); return;
}

function comando($nick, $msg){
    global $quienes;
    $cmd = "Los comandos que de vaina me se: !cuantos !sonando !cita [cita] !recita !dedicatoria [sujeto] !invitar";
    if ($msg=="!color") { priv ("\u000304 color \x034color"); return; }
    if ($msg=="!comandos" || $msg=="!ayuda") { priv($cmd); return; }
    if ($msg=="!sonando") { sonando(); return; }
    if ($msg=="!cuantos") { cuantos($nick); return; }
    if ($msg=="!recita") { recita(); return; }
    if ($msg=="!invitar") { quienes(); invitar($quienes, $nick); return; }
    if (preg_match("/^(!cita) (.+)$/", $msg)) {
        cita($nick, mb_substr($msg, 6));
        return;
    }
    if (preg_match("/^(!hablaclaro) (.+)$/", $msg)) {
        hablaclaro(mb_substr($msg, 12));
        return;
    }
    if (preg_match("/^(!dedicatoria) (.+)$/", $msg)) {
        dedicatoria(mb_substr($msg, 13));
        return;
    }
    if (preg_match("/^(!identica) (.+)$/", $msg)) {
        dent($nick, mb_substr($msg, 10), "radio");
        return;
    }
    if (preg_match("/^(!gnolldent) (.+)$/", $msg)) {
        dent($nick, mb_substr($msg, 11), "gnoll");
        return;
    }
}

function quienes() {
    global $conf;
    $canal = $conf['canal'];
    send("NAMES $canal\n");
    return;
}

function invitar ($q, $nick) {
    global $conf, $quienes;
    priv ("Comienza un programa en vivo, escúchalo en http://is.gd/fyUqWE");
    foreach (split(" ", $quienes) as $q)
        if ($q && $q!=$conf['nick'] && in_array($nick, $conf['admins'])) {
            $quita = array("@" => "", "~" => "", "+" => "");
            $q = strtr($q, $quita);
            send ("PRIVMSG $q te manda a decir $nick que comienza un programa en vivo, así que corre la voz por las redes sociales poniendo este dent: \"Comienza otro programa en vivo en !radiognu. Corre la voz y escúchalo en http://ur1.ca/2ohji :-)\"\n");
            sleep (1);
        }
}

function dent ($nick, $dent, $usuario) {
    global $identica_radiognu, $conf;
    if ($usuario == "radio"){
        if (in_array($nick, $conf['admins'])) {
            //$identica_radiognu->updateStatus($dent);
            priv ("DENT escrito en @radiognu: \"$dent\"");
        } else {
            priv ("$nick, si no hubiese tanto echadorcito de vaina, pudiera dejar que todos enviaran dents en @radiognu");
        }
    }
}

function responder($nick, $msg) {
    $frase = archivo("nombrado");
    $say = preg_replace("/____NICK____/", $nick, $frase);
    priv($say);
}

function sonando() {
    $json = json_decode(file_get_contents("http://radiognu.org/api?no_cover"));
    if ($json->isLive) {
        $salida = "EN VIVO: «".$json->show."» de ".$json->broadcaster.
            " transmite “".$json->title."” de ‘".$json->artist."’";
    } else {
        $salida = "SONANDO EN DIFERIDO: “".$json->title."” de ‘".$json->artist.
            "’ del álbum «".$json->album."» (".$json->country.", ".$json->year.
            ", ".$json->license->shortname.")";
    }
    priv($salida);
}

function cuantos($nick) {
    $json = json_decode(file_get_contents("http://radiognu.org/api?no_cover"));
    $total  = $json->listeners;
    $frase  = archivo ("cuantos");
    $say    = preg_replace("/_X_/", $total, $frase);
    $say    = preg_replace("/____NICK____/", $nick, $say);
    priv ($say);
}

function dedicatoria($sujeto) {
    $frase = archivo ("dedicatoria");
    $say = preg_replace("/____NICK____/", $sujeto, $frase);
    priv ("\x02\x032".$say);
    return;
}

function cita($nick, $quota) {
    $filename = './textos/citas.txt';
    $fecha  = date("D M j G:i:s Y");
    $cita = "$fecha || $nick || $quota\n";
    if (is_writable($filename)) {
        if (!$handle = fopen ($filename, 'a')) {
            echo "ERROR: No se puede abrir el archivo ($filename)";
            return;
        }
        if (fwrite($handle, $cita) === FALSE) {
            echo "ERROR: No se puede escribir sobre el archivo ($filename)";
            return;
        } else {
            priv ("CITA escrita: $quota");
        }
        fclose($handle);
    } else {
        echo "ERROR: El archivo $filename no tiene permisos de escritura";
    }
}

function recita() {
    $linea = archivo("citas");
    $l = split("\|\|", $linea); // 0 = fecha; 1 = quien; 2 = cita
    $l[1] = trim($l[1]);
    $salida = "Cita de {$l[1]} en fecha {$l[0]}:";
    priv("\x032".$salida);
    $salida = "{$l[2]}";
    priv("\x032".$salida);
}

function hablaclaro($nick) {
    $frase  = archivo ("hablaclaro");
    $say = "$nick: HABLA CLARO ".$str = mb_convert_case($frase, MB_CASE_TITLE, "UTF-8");
    priv ("\x02\x038".$say);
    return;
}

function reconocer($nick, $msg) {
    $groserias = array("marico", "marica", "puta", "puto", "pajuo", "pajua",
                       "mamaguevo", "culo", "mierda");
    $palabras = split(" ", $msg);
    $coincide = array_intersect($groserias, $palabras);
    if (sizeof($coincide)>0) {
        priv("$nick: pudieras dar esa magistral demostracion de lexico con menos frecuencia?");
        return;
    }
}

function priv($msg) {
    global $SOCKET, $conf;
    $dest = $conf['canal'];
    $mensaje = "PRIVMSG $dest :$msg\n";
    @fwrite($SOCKET, $mensaje, strlen($mensaje));
    if ($conf['debug']['priv'] == 1) { echo "[PRIV] $mensaje\n"; }
}

function send($cmd) {
    global $SOCKET, $conf;
    @fwrite($SOCKET, $cmd, strlen($cmd));
    if ($conf['debug']['send'] == 1) { echo "[SEND] $cmd\n"; }
}

function archivo($com) {
    $file = "textos/$com.txt";
    $archivo = file($file);
    $azar = rand(0, sizeof($archivo)-1);
    $salida = $archivo[$azar];
    return $salida;
}

function loguear ($nick, $msg) {
    $all = './log/all.txt';
    $act = './log/act.txt';
    $fecha  = date("M j G:i:s");
    $log  = "$fecha <$nick> $msg\n";
    if (is_writable($all) && is_writable($act)) {
        if (!$handle = fopen ($all, 'a')) {
            echo "ERROR: No se pudo abrir el archivo ($all)";
            return;
        }
        if (fwrite($handle, $log) === FALSE) {
            echo "ERROR: No se puede escribir sobre el archivo ($all)";
            return;
        }
        fclose($handle);
        if (!$handle = fopen ($act, 'a')) {
            echo "ERROR: No se puede abrir el archivo ($act)";
            return;
        }
        if (fwrite($handle, $log) === FALSE) {
            echo "Cannot write to file ($act)";
            return;
        }
        fclose($handle);
    } else {
        echo "ERROR: Los archivos '$all' o '$act' no tienen permisos de escritura";
    }
}
?>
