hazle una dedicatoria a éste.
dedícame éste.
No existe nada malo en el mundo; es ____NICK____ quien lo hace aparecer así.
Cuando no se piensa lo que se dice es cuando se dice lo que piensa ____NICK____.
Si la existencia inútil fuese una flor, ____NICK____ sería la primavera.
Nadie es perfecto, y ____NICK____ es un perfecto ejemplo.
Los camaleones son verdes y cambian de color, pero a ____NICK____ no lo cambia ni Dios.
Nadie merece una burla, a menos que sea ____NICK____.
____NICK____ es un alago con envidia.
____NICK____ es un arma de doble filo.
Detras de cada ____NICK____ hay algo en serio.
El que busca a ____NICK____ corre el riesgo de encontrarle.
Como todos los soñadores, ____NICK____ confunde el desencanto con la verdad.
La verdad triunfa por sí misma, ____NICK____ necesita siempre complicidad.
Si tu intención es describir a ____NICK____, hazlo con sencillez y la elegancia déjasela al sastre.
Si no actúas como piensas, vas a terminar pensando como ____NICK____.
Una colección de dedicatorias a ____NICK____ debe ser una farmacia donde se encuentra remedio a todos los males.
Quien no quiere pensar es un fanático; quien no puede pensar, es un idiota; quien no osa pensar es ____NICK____.
Búrlate de ____NICK____ cuando seas perfecto.
miren a ____NICK____: .... no.... mejor ni lo miren
no es que ____NICK____ tenga alguna razón por la cual dedicarle algo. Es que no la tiene.
No. suficientes !dedicatorias ya
Se imaginan a ____NICK____ perreando solo frente a un espejo todo borracho?
no se por que a la gente le da por invocar dedicatorias a ____NICK____. No tiene sentido
Solamente aquel que es demasiado fuerte para perdonar una ofensa sabe hacerle !dedicatorias a ____NICK____.
Se habrán dicho muchas cosas, se habrán escrito muchos libros, pero una buena !dedicatoria a ____NICK____ no se he hecho nunca.
No nos burlemos de ____NICK____, sino de lo que sus padres hicieron.
Suelo recordar bien a las personas y las cosas, pero con ____NICK____ voy a hacer una excepcion.
Si la burla fuera un pecado capital, la existencia de ____NICK____ es una condena directa.
Lero, lero, no soy como ____NICK____.
____NICK____ es una paradoja resuelta.
Ha llegado el momebto de dejar de reirse de ____NICK____.
No nos reimos de ____NICK____, sino con él, lo que pasa es que él no colabora.
____NICK____ esta en el canal porque le dijeron que aqui no excluiamos a gente con problemas graves de conducta
si me preguntan que es peor entre ____NICK____ y Carmona Estanga... digo que ____NICK____, por supuesto
quien crea que puede llegar a hacer algo productivo conversando con ____NICK____: que se baje de esa nube
es inutil meterse con ____NICK____: no tiene sentido perder el tiempo asi
es logico pensar viendo a ____NICK____ que es fatal vivir demasiado tiempo
Tipico piropo para ____NICK____: "Te quiero como eres. Pero no me digas como eres, por favor"
Aquel que quiere ser como ____NICK____, vive la pesadilla de poder ser asi algun dia
____NICK____ es el espejo en donde se refleja la derrota de todo nuestro sistema cultural
ser ____NICK____ no es pecar, es pecar sin remordimiento
____NICK____ es un mendigo que pide que su vida tenga sentido con tanta insistencia como la necesidad, pero mucho mas insaciable
uno sabe lo que es el paraiso despues de imaginar que ____NICK____ no estuviera aca
____NICK____ es una loca pretension de ser humano
____NICK____ es mas razonador que razonable
Los seres como ____NICK____ nunca seran esclavos; son libres para hacer cualquier cosa que el Gobierno o la opinion publica les permitan
____NICK____ es el antonimo de poesia
____NICK____ no refrena su lengua ni sigue a los dioses
____NICK____ es tan pavoso que revuelve el fuego con un cuchillo
Todo llega si uno simplemente espera... con la excepcion de que ____NICK____ deje de hablar en este canal
Cuando oigas a ____NICK____ hablar de su amor por la patria, es signo de que espera que le paguen por eso
No tomes el nombre de Dios en vano; escoge el momento en que tenga efecto adverso sobre ____NICK____
Cuando Dios creo el mundo vio que era bueno. Que dira ahora con ____NICK____ en el IRC?
Cuando se deja de creer en Dios, enseguida se cree en cualquier cosa, como por ejemplo que la vida de ____NICK____ tiene sentido
La violencia no puede jamas persuadir a los hombres; solo logra hacerlos como ____NICK____.
Si no puedes ser lo que eres, se con sinceridad lo que puedas; si no puedes hacer mas nada: se como ____NICK____
Las opinions de ____NICK____ en cuanto tienen que ver con la realidad, no son ciertas; y en cuanto que son ciertas, no tienen que ver con la realidad
Miren a ____NICK____: ha conseguido pasar de la nada a la mas absoluta incoherencia humana
____NICK____ es un mal necesario
Desdichado el que por tal se tiene: ____NICK____ se tiene por tal.
Todo lo que se ignora, se desprecia: por eso ignoramos a ____NICK____
El que no se atreve a ser inteligente es como ____NICK____
como ____NICK____ nunca cree en lo que dice, se sorprende cuando alguien si cree sus sandeces
a cada cochino le sale su ____NICK____
____NICK____ no es gente. Punto.
Si ____NICK____ tuviera problemas sexuales mas evidentes de los que evidencia: tampoco importaria mucho
____NICK____ muestra solo un poco de su irresponsabilidad, el resto se sobreentiende con su ilogica existencia
____NICK____ es asfixiante
no estes triste: contempla a ____NICK____ y alegrate de no ser asi
la alegria es el sentimiento de saberse distinto a ____NICK____
el que rie de ultimo rie mejor, pero ____NICK____ nunca entiende el chiste
____NICK____ no es inutil: sirve de mal ejemplo
____NICK____ nunca podra estudiar derecho: esta demasiado torcido
el que sabe sabe; el que no sabe es ____NICK____
la gente que no tiene suerte con ____NICK____ no sabe la suerte que tiene
cuando uno ve a gente como ____NICK____ se puede sentir un alivio viendo la diferencia
mas o menos a cuantos metros de distancia quisieras estar de ____NICK____. Saca esa cuenta y alegrate.
Pocos creen que pueden alcanzar la delicada condición de ____NICK____.
Da tranquilidad saber que hay una sola persona como ____NICK____ en todo el mundo.
